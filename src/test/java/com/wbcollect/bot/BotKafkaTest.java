package com.wbcollect.bot;

import com.wbcollect.initializers.KafkaInitializer;
import com.wbcollect.initializers.MockServerInitializer;
import com.wbcollect.proto.Topics;
import com.wbcollect.proto.util.ProtoUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockserver.client.MockServerClient;
import org.mockserver.model.HttpResponse;
import org.mockserver.springtest.MockServerTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.notFoundResponse;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.model.HttpStatusCode.SERVICE_UNAVAILABLE_503;

@RunWith(SpringRunner.class)
@SpringBootTest
@Testcontainers
@MockServerTest("server.url=http://localhost:${mockServerPort}")
@ContextConfiguration(initializers = {
        KafkaInitializer.class,
        MockServerInitializer.class,
})
public class BotKafkaTest {

    @Value("${server.url}")
    private String serverUrl;

    private MockServerClient mockServerClient;

    private static final AtomicInteger offset = new AtomicInteger(0);

    @Container
    private static final KafkaContainer KAFKA_CONTAINER = KafkaInitializer.KAFKA_CONTAINER;

    @Container
    private static final MockServerContainer MOCK_SERVER_CONTAINER = MockServerInitializer.MOCK_SERVER_CONTAINER;

    @Autowired
    public KafkaTemplate<String, byte[]> kafkaTemplate;

    @Autowired
    public ConsumerFactory<String , byte[]> consumerFactory;

    @BeforeEach
    void init() {
        kafkaTemplate.setConsumerFactory(consumerFactory);
    }


    @Test
    public void sendOneTaskToInboundTopic_serverSuccessfullyReturnsResult() {
        String path = "/catalog/12345";
        mockServerClient
                .when(request().withPath(path))
                .respond(response()
                        .withStatusCode(200)
                        .withBody("some product data")
                );

        String testUrl = serverUrl + path;
        var payload = BotProto.InboundTask.newBuilder()
                .setId(System.currentTimeMillis())
                .setUrl(testUrl)
                .build();
        kafkaTemplate.send(Topics.BOT.INBOUND_TASK_NAME, ProtoUtils.toByteArrayDelimitedOrThrow(payload));
        var response = kafkaTemplate.receive(Topics.BOT.OUTBOUND_TASK_NAME, 0, offset.getAndIncrement());

        assertNotNull(response);
        BotProto.OutboundTask outboundTask = ProtoUtils.parseOrThrow(response.value(), Topics.BOT.OUTBOUND_TASK_CLASS);
        assertEquals(200, outboundTask.getStatus());
        assertEquals("some product data", outboundTask.getData());
    }

    @Test
    public void sendOneTaskToInboundTopic_serverReturns404() {
        String path = "/catalog/123";
        mockServerClient
                .when(request().withPath(path))
                .respond(notFoundResponse().withBody("an error"));

        String testUrl = serverUrl + path;
        var payload = BotProto.InboundTask.newBuilder()
                .setId(System.currentTimeMillis())
                .setUrl(testUrl)
                .build();
        kafkaTemplate.send(Topics.BOT.INBOUND_TASK_NAME, ProtoUtils.toByteArrayDelimitedOrThrow(payload));
        var response = kafkaTemplate.receive(Topics.BOT.OUTBOUND_TASK_NAME, 0, offset.getAndIncrement());
        assertNotNull(response);
        BotProto.OutboundTask outboundTask = ProtoUtils.parseOrThrow(response.value(), Topics.BOT.OUTBOUND_TASK_CLASS);
        assertEquals(404, outboundTask.getStatus());
        assertEquals("an error", outboundTask.getData());
    }

    @Test
    public void sendOneTaskToInboundTopic_serverReturns503() {
        String path = "/catalog/123";
        mockServerClient
                .when(request().withPath(path))
                .respond(new HttpResponse()
                        .withStatusCode(SERVICE_UNAVAILABLE_503.code())
                        .withReasonPhrase(SERVICE_UNAVAILABLE_503.reasonPhrase())
                );

        String testUrl = serverUrl + path;
        var payload = BotProto.InboundTask.newBuilder()
                .setId(System.currentTimeMillis())
                .setUrl(testUrl)
                .build();
        kafkaTemplate.send(Topics.BOT.INBOUND_TASK_NAME, ProtoUtils.toByteArrayDelimitedOrThrow(payload));
        var response = kafkaTemplate.receive(Topics.BOT.OUTBOUND_TASK_NAME, 0, offset.getAndIncrement());
        assertNotNull(response);
        BotProto.OutboundTask outboundTask = ProtoUtils.parseOrThrow(response.value(), Topics.BOT.OUTBOUND_TASK_CLASS);
        assertEquals(503, outboundTask.getStatus());
    }
}

