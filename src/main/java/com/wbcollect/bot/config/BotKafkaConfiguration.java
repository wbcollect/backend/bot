package com.wbcollect.bot.config;

import com.wbcollect.bot.web.handler.RestTemplateResponseErrorHandler;
import com.wbcollect.common.KafkaCustomAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

@EnableTransactionManagement
@Import({KafkaCustomAutoConfiguration.class})
@Configuration
public class BotKafkaConfiguration {
    /**
     * Configure RestTemplate to properly handle api calls
     */
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                .errorHandler(new RestTemplateResponseErrorHandler())
                .build();
    }
}
