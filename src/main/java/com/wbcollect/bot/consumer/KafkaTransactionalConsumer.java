package com.wbcollect.bot.consumer;

import com.wbcollect.bot.service.BotKafkaService;
import com.wbcollect.proto.Topics;
import com.wbcollect.proto.util.ProtoUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import static com.wbcollect.common.kafka.KafkaConst.DLQ_ERROR_HANDLER;
import static com.wbcollect.common.kafka.KafkaConst.SINGLE_KAFKA_LISTENER_CONTAINER_FACTORY;

@Slf4j
@RequiredArgsConstructor
@Component
public class KafkaTransactionalConsumer {
    private final BotKafkaService botKafkaService;

    /**
     * This consumer calls transactional service method
     */
    @KafkaListener(
            topics = Topics.BOT.INBOUND_TASK_NAME,
            containerFactory = SINGLE_KAFKA_LISTENER_CONTAINER_FACTORY,
            errorHandler = DLQ_ERROR_HANDLER
    )
    public void onInboundTask(@Payload final byte[] payload) {
        var inboundTask = ProtoUtils.parseOrThrow(payload, Topics.BOT.INBOUND_TASK_CLASS);
        log.info("Received task, id: " + inboundTask.getId() + ", url: " + inboundTask.getUrl());
        botKafkaService.getAndSendDataToKafka(inboundTask);
    }
}
