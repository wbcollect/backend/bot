package com.wbcollect.bot.web.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Slf4j
@Component
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {
    /**
     * All api responses with errors are handled by com.wbcollect.worker microservice
     */
    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
        if (httpResponse.getStatusCode()
                .series() == HttpStatus.Series.SUCCESSFUL) {
            log.info("Successfully received data.");
        } else if (httpResponse.getStatusCode()
                .series() == HttpStatus.Series.SERVER_ERROR) {
            log.error("Server error: " + httpResponse.getStatusText());
        } else if (httpResponse.getStatusCode()
                .series() == HttpStatus.Series.CLIENT_ERROR) {
            log.error("Client error: " + httpResponse.getStatusText());
        } else {
            log.error("Unknown error: " + httpResponse.getStatusText());
        }
        return false;
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse) {
    }
}