package com.wbcollect.bot.service;

import com.wbcollect.bot.BotProto;
import com.wbcollect.proto.Topics;
import com.wbcollect.proto.util.ProtoUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ThreadLocalRandom;

@Slf4j
@Service
@RequiredArgsConstructor
public class BotKafkaService {
    private final RestTemplate restTemplate;
    private final KafkaTemplate<String, byte[]> kafkaTemplate;

    /**
     * With a transaction in place:
     * <p>
     * 1. Calls a third party service.
     * <p>
     * 2. Sends an outbound event.
     */
//    @Transactional
    public void getAndSendDataToKafka(BotProto.InboundTask inboundTask) {
        try {
            Thread.sleep(ThreadLocalRandom.current().nextLong(150, 300));
        }
        catch (InterruptedException e) {
            log.error("Cannot freeze bot, ", e);
        }
        var response = restTemplate.getForEntity(inboundTask.getUrl(), String.class);
        var body = response.getBody() == null ? "" : response.getBody();
        var outboundTask = BotProto.OutboundTask.newBuilder()
                .setId(inboundTask.getId())
                .setUrl(inboundTask.getUrl())
                .setData(body)
                .setStatus(response.getStatusCodeValue())
                .build();
        kafkaTemplate.send(Topics.BOT.OUTBOUND_TASK_NAME, ProtoUtils.toByteArrayDelimitedOrThrow(outboundTask));
        log.info("Response with id: " + outboundTask.getId() + " sent to " + Topics.BOT.OUTBOUND_TASK_NAME);
    }
}
